#!/usr/bin/env bash

RED='\033[0;31m'
MAGENTA='\e[35m'
NC='\033[0m'

function usage {
    echo -e "$NC$(/usr/bin/basename "$0") [-h | --help] [-p | --path] [-n | --name]

Script to validate .gitlab-ci.yaml

where:
    -n, --name  name of GitLab CI config file (default: .gitlab-ci.yml)
    -p, --path  path to GitLab CI config file (default: this directory).
    -h, --help  displays this help.
"
}

function check_or_set_parameters {
    while [ "$#" -gt 0 ]; do
        case "$1" in
        -h | --help)
            usage
            exit 1
            ;;
        -p | --path)
            CI_PATH=$2
            shift
            ;;
        -n | --name)
            CI_NAME=$2
            shift
            ;;
        *)
            echo -e "${RED}Unknown parameter passed: $1$NC"
            usage
            exit 1
            ;;
        esac
        shift
    done

    if [[ -z "$CI_NAME" ]]; then
        CI_NAME=".gitlab-ci.yml"
    fi

    if [[ -z "$CI_PATH" ]]; then
        CI_PATH="./$CI_NAME"
    else
        CI_PATH="$CI_PATH/$CI_NAME"
    fi
}

function check_dependencies {
    if ! command -v gem &>/dev/null; then
        read -r -p "${MAGENTA}rubygems isn't installed. Install it? [y/N]$NC" response
        response=${response,,}
        if [[ "$response" =~ ^(yes|y)$ ]]; then
            sudo apt-get install rubygems
        else
            echo 'BYE!' >&2
            exit 0
        fi
    fi

    if ! command -v gem list -i gitlab-lint-client &>/dev/null; then
        read -r -p "${MAGENTA}Gem 'gitlab-lint-client' isn't installed. Install it? [y/N]$NC" response
        response=${response,,}
        if [[ "$response" =~ ^(yes|y)$ ]]; then
            sudo gem install gitlab-lint-client
        else
            echo 'BYE!' >&2
            exit 0
        fi
    fi
}

function check_gitlab_ci {
    check_dependencies
    glab-lint -f $CI_PATH -u https://gitlab.com
}

check_or_set_parameters "$@"
check_dependencies
check_gitlab_ci
exit
